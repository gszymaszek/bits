Title: Invitation to the MiniDebConf 2014 Barcelona: 15-16 March 2014
Date: 2014-02-24 12:00
Tags: minidebconf, debian women, announce
Slug: minidebconf-barcelona-announce
Author: Mònica Ramírez Arceda
Status: published

Debian Women will hold a [MiniDebConf](http://bcn2014.mini.debconf.org/) in
Barcelona on **Saturday 15th and Sunday 16th of March, 2014**.
Everyone is invited to both talks and social events, but the speakers will all
be people who identify themselves as female. This is not a conference about
women in Free Software, or women in Debian, rather a usual Debian Mini-DebConf
where all the speakers are women.

The
[talks schedule](https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/Talks)
has already been published. It is going to be an exciting event, packed with
interesting talks for all audiences, in a beautiful
[venue](http://bcn2014.mini.debconf.org/venue.shtml), in one of the most famous
European cities.

Registration is not mandatory, but strongly encouraged, as it helps the event's
organisation and logistics. Please,
[register in the wiki](https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/Participants).

We are still raising funds to cover the costs of running the conference and to
offer travel sponsorship to people who can't pay for it. Please, consider
donating any amount you can in our
[crowd-funding campaign](https://freedomsponsors.org/core/issue/427/), or
[contact us](mailto:debian-miniconf@cpl.upc.edu) if you would like to become a
sponsor.

The conference organisers want to thank the organisations that have already
became sponsors, making this event possible, and specially our Platinum
sponsor, **[Google](http://google.com)**; our Gold sponsors,
**[Càtedra de Programari Lliure - Universitat Politècnica de
Catalunya](http://cpl.upc.edu)**, **[Blue Systems](http://blue-systems.com)**
and our Silver sponsors, **[CAtalan LInux Users](http://caliu.cat), [CAPSiDE](http://capside.com/en/)**
and **[Fluendo](http://fluendo.com)**.

For more information, visit the website of the event:
[http://bcn2014.mini.debconf.org](http://bcn2014.mini.debconf.org)
