Title: Debian 8.0 Jessie has been released!
Date: 2015-04-26 03:15
Tags: jessie
Slug: jessie-released
Author: Ana Guerrero Lopez
Status: published


![Alt Jessie has been released](|static|/images/banner_jessie.png)

There's a new sheriff in town. And her name is Jessie. We're happy to announce
the release of Debian 8.0, codenamed *Jessie*.

**Want to install it?**
Choose your favourite [installation media](https://www.debian.org/distrib/) among Blu-ray Discs, DVDs,
CDs and USB sticks. Then read the [installation manual](https://www.debian.org/releases/jessie/installmanual).
For cloud users Debian also offers [pre-built OpenStack images](http://cdimage.debian.org/cdimage/openstack/current/) ready to use.

**Already a happy Debian user and you only want to upgrade?**
You are just an *apt-get dist-upgrade* away from Jessie!
Find how, reading the [installation guide](https://www.debian.org/releases/jessie/installmanual)
and the [release notes](https://www.debian.org/releases/jessie/releasenotes).

**Do you want to celebrate the release?**
Share the [banner from this blog](https://wiki.debian.org/DebianArt/Themes/Lines#Release_blog_banner.2Fbutton) in your blog or your website!

