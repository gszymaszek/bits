Title: Debian Contributors Survey 2016
Slug: debian-contributors-survey-2016
Date: 2016-11-16 15:45
Author: Molly de Blanc
Tags: announce, survey, contributing
Status: published

The [Debian Contributor Survey][1] launched [last week][3]!

[3]: https://lists.debian.org/debian-devel-announce/2016/11/msg00003.html

In order to better understand and document who contributes to Debian, we
(Mathieu ONeil, Molly de Blanc, and Stefano Zacchiroli) have created this
survey to capture the current state of participation in the Debian Project
through the lense of common demographics. We hope a general survey will become
an annual effort, and that each year there will also be a focus on a specific
aspect of the project or community. The 2016 edition contains sections
concerning work, employment, and labour issues in order to learn about who is
getting paid to work on and with Debian, and how those relationships affect
contributions.

[1]: http://debian.limequery.org/696747

We want to hear from as many Debian contributors as possible—whether you've
submitted a bug report, attended a DebConf, reviewed translations, maintain
packages, participated in Debian teams, or are a Debian Developer. Completing
the survey should take 10-30 minutes, depending on your current involvement
with the project and employment status.

In an effort to reflect our own ideals as well as those of the Debian project,
we are using [LimeSurvey][2], an entirely free software survey tool, in an
instance of it hosted by the LimeSurvey developers.

Survey responses are anonymous, IP and HTTP information are not logged, and all
questions are optional. As it is still likely possible to determine who a
respondent is based on their answers, results will only be distributed in
aggregate form, in a way that does not allow deanonymization. The results of
the survey will be analyzed as part of ongoing research work by the organizers.
A report discussing the results will be published under a DFSG-free license and
distributed to the Debian community as soon as it's ready. The raw,
disaggregated answers will not be distributed and will be kept under the
responsibility of the organizers.

[2]: http://limesurvey.org/

We hope you will [fill out the Debian Contributor Survey][1]. The deadline for
participation is: 4 December 2016, at 23:59 UTC.

If you have any questions, don't hesitate to contact us via email at:

* Mathieu ONeil <mailto:mathieu.oneil@canberra.edu.au>
* Molly de Blanc <mailto:deblanc@riseup.net>
* Stefano Zacchiroli <mailto:zack@debian.org>
