Title: I love Free Software Day 2016: Muestra tu amor por el Software Libre
Slug: ilovefs-2016
Date: 2016-02-14 01:10
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz
Lang: es
Tags: contributing, debian, free software, FSFE
Status: published

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-es.png)][ilovefs-link]

Hoy, 14 de Febrero, la Fundación del Software Libre de Europa (FSFE)
celebra el [día «I Love Free Software»][ilovefs-link]. Éste es un día
para que los usuarios de Software Libre puedan apreciar y agradecer
a quienes contribuyen a sus programas, proyectos y organizaciones
favoritas.

Aprovechamos esta oportunidad para dar las gracias a todos los autores cuyo
software se distribuye en Debian, a todos aquellos que trabajan en
distribuciones derivadas, y por supuesto, a todos los desarrolladores
y colaboradores de Debian. ¡Gracias por vuestro trabajo y dedicación
al software libre!


Hay muchas formas de participar en este día «ILoveFS» y animamos a
todos a unirse y celebrarlo. Muestra tu amor hacia los desarrolladores,
colaboradores y equipos de Debian, de forma virtual en las redes
sociales usando el «hashtag» #ilovefs y difundiéndolo en tus propios
círculos. También puedes visitar la página web [ILoveFS](http://ilovefs.org) para
encontrar material promocional tal como postales o _banners_.

Para conocer más sobre la FSFE, puedes leer su [nota de la campaña][announcement-link]
o visitar su [página web][fsfe-link].
[ilovefs-link]: https://fsfe.org/campaigns/ilovefs/2016/
[announcement-link]: https://fsfe.org/news/2016/news-20160208-01.es.html
[fsfe-link]: https://fsfe.org
