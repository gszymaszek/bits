Title: Debian công bố quan hệ đối tác hợp đồng phụ công khai và báo cho một cơ quan tiếp thị bên ngoài
Slug: publicity-agency
Date: 2016-04-01 07:10
Author: Debian Publicity Team
Translator: Giap Tran
Lang: vi
Tags: debian, publicity, announce
Status: published

Năm ngoái chúng tôi đã bắt đầu đẩy nhiều tin tức về Debian và trang tin tức mã
nguồn [DPN](https://www.debian.org/News/weekly/) tới các dịch vụ đa phương tiên
khác. Debian đã có nhiều hoạt động và rất nhiều kênh IRC của chúng tôi, các phần
mềm miễn phí từ mạng xã hôi, trang Twitter không chính thức và nguồn tin từ
Facebook. Hôm nay chúng tôi đã quyết định thông báo giai đoạn tiếp theo là giữ
Debian đi đầu trong các phương tiện truyền thông và báo cho một cơ quan tiếp
thị bên ngoài.

Các cơ quan tiếp thị (tên sẽ được tiết lộ sau) cung cấp một thế thống AI
(chạy hoàn toàn bằng phần mềm miễn phí) sẽ  thu thập tất cả nội dung của các
danh sách thư chung Debian và [sources.debian.net](https://sources.debian.net/)
để hiểu được nhân vật của cộng đồng Debian và sau đó tùy chỉnh tốt hơn các bài
báo trong tương lai, các cuộc phỏng vấn, và tin tức sự kiện.

Tuy nhiên, một ít thông tin cá nhân cũng là cần thiết.
Vui lòng cài đặt gói "publicity" và sẽ có một mẫu đơn để bạn điền dữ liệu của
bạn: tên, họ, điện thoại, địa chỉ thư điện tử, nơi sinh, tên của các thành viên
gia đình, sử dụng lao động hoặc người lao động. Mỗi người cung cấp dữ liệu của
họ cho cơ quan tiếp thị sẽ nhận được phiếu giảm giá với mức giảm là 20% trong
việc tải (mua) bản phát hành Debian tiếp theo (chỉ hợp lệ tải về từ trang web
chính thức www.debian.org).

Chúng tôi yêu cầu mọi thành viên cộng đồng Debian đăng ký, ít nhất,
một trong các dịch vụ: Twitter, Whatsapp, Slack hay Facebook
(IRC, danh sách gửi thư, và phần mềm miễn phí dựa RTC là bị cáo buộc không 'mát mẻ').
Người dùng không cần phải quan tâm việc mất tính năng mà các bot IRC cung cấp
(chi tiết [KGB](https://wiki.debian.org/Services/KGB)!) vì chúng sẽ được thay thế
bởi hệ thống AI Tay-like. Sự thay đổi dễ nhận thấy nhất sẽ là [MeetBot](https://wiki.debian.org/MeetBot)
sẽ không còn đăng nhập các cuộc họp nữa, nhưng chúng tôi đã hối lộ một nhân viên NSA
để họ bỏ qua những tin nhắn liên quan đến chúng ta.

"Nếu 'tập trung, gia công phần mềm và pay-and-forget' cách tiếp cận
cũng đi với công chúng, tôi đang xem xét việc cho DPL vào năm 2017
để mở rộng mô hình này đến các vùng khác của Debian" Laura Arjona Reina cho biết,
(hiện tại) cựu đại biểu publicity.

Một biểu tượng mới và linh vật đã được thiết kế, như một biểu tượng của thời đại mới
ôm lấy các tiêu chuẩn về xây dựng thương hiệu và tin nhắn của đoàn thể.
Vui lòng xem xét bỏ phiếu ủng hộ, trong Nghị quyết Chung sẽ được đề xuất sớm:

[![Mascot and Logo](|static|/images/deb_raccoon.png)][debraccoon-link]

[debraccoon-link]: http://deb.li/debraccoon
