Title: Acabamos de publicar Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: gl
Translator: Alba Vidal Fernández

![Alt Acabamos de publicar Stretch](|static|/images/banner_stretch.png)

Déixate abrazar polos elásticos tentáculos de goma do polbo de xoguete lila! Estamos encantadas de anunciar que se acaba de publicar Debian 9.0, de sobrenome *Stretch*.

**Quérela instalar?**
Escolle o [medio de instalación](https://www.debian.org/distrib/) que prefiras de entre discos Blu-ray, DVDs, CDs e memorias USB. E despois consulta o [manual de instalación](https://www.debian.org/releases/stretch/installmanual).

**Xa tes Debian a funcionar felizmente no teu sistema e só queres actualizar?**
Pódelo facer desde Debian 8 Jessie sen problema: consulta as [notas de publicación](https://www.debian.org/releases/stretch/releasenotes).

**Queres darlle a benvida a Stretch?**
Comparte a alegría no teu blog ou na túa web co [deseño provisto neste blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png)!
