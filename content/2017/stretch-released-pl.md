Title: Wydano Debiana 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: pl
Translator: Michał Kułach

![Alt Stretch wydany](|static|/images/banner_stretch.png)

Wpadnij w objęcia fioletowej, gumowej ośmiornicy-zabawki! Z radością ogłaszamy wydanie Debiana 9.0 o nazwie kodowej *Stretch*.

**Chcesz go zainstalować?**
Wybierz swój ulubiony [nośnik](https://www.debian.org/distrib/) spośród USB oraz płyt Blu-ray, DVD i CD. A później zajrzyj do [podręcznika instalacji](https://www.debian.org/releases/stretch/installmanual).

**Jesteś już szczęśliwym użytkownikiem Debiana i chcesz go tylko zaktualizować?**
Aktualizację możesz przeprowadzić z łatwością z poziomu Debiana 8 Jessie, zapoznaj się tylko z [uwagami do wydania](https://www.debian.org/releases/stretch/releasenotes).

**Chcesz świętować nowe wydanie?**
Umieść [baner z tego bloga](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) na swoim blogu lub stronie internetowej!
