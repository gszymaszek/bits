Title: 25 anos e contando
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday, aniversário
Lang: pt-BR
Translator: Daniel Lenharo de Souza
Status: published

![Debian 25 anos por Angelo Rosa](|static|/images/debian25years.png)

Quando Ian Murdock (em memória) anunciou há 25 anos em comp.os.linux.development,
*"a conclusão iminente de um novo lançamento Linux, [...] a Distribuição
Debian Linux"*, ninguém esperaria que o "A distribuição Debian Linux" se
tornasse o que hoje em dia é conhecido como o Projeto Debian, um dos maiores
e mais influentes projetos de software livre. Seu principal produto é o Debian,
um sistema operacional livre (SO) para o seu computador, bem como para muitos
outros sistemas que melhoram sua vida. Do funcionamento interno do seu
aeroporto mais próximo ao sistema de entretenimento do carro,dos servidores
em nuvem que hospedam seus sites favoritos aos dispositivos de IoT que se
comunicam com eles, o Debian pode alimentar tudo.

Hoje, o projeto Debian é uma organização grande e próspera, com inúmeros
times auto-organizados formados por voluntários. Embora muitas vezes pareça
caótico do lado de fora, o projeto é sustentado por seus dois principais
documentos organizacionais: o [Contrato Social Debian], que fornece uma visão
de melhoria da sociedade, e a [Definição debian de Software Livre (DFSG)],
que fornece uma indicação do qual software é considerado utilizável. Eles
são complementados pela [Constituição] do projeto, que estabelece a estrutura
do projeto, e o [Código de Conduta], que define o tom das interações dentro
do projeto.

Todos os dias, nos últimos 25 anos, as pessoas enviaram relatórios e
correções de erros, enviaram pacotes, atualizaram traduções, criaram
ilustrações, organizaram eventos sobre o Debian, atualizaram o site,
ensinaram outros a usar o Debian e criaram centenas de distribuições derivadas.

**Estamos prontos para mais 25 anos - e esperamos muitos mais!**

[Contrato Social Debian]: https://www.debian.org/social_contract.pt
[Definição debian de Software Livre (DFSG)]: https://www.debian.org/social_contract.pt.html#guidelines
[Constituição]: https://www.debian.org/devel/constitution.pt
[Código de Conduta]: https://www.debian.org/code_of_conduct.pt
