Title: Novos desenvolvedores e mantenedores Debian (setembro e outubro de 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos últimos dois meses:

  * Joseph Herlant (aerostitch)
  * Aurélien Couderc (coucouf)
  * Dylan Aïssi (daissi)
  * Kunal Mehta (legoktm)
  * Ming-ting Yao Wei (mwei)
  * Nicolas Braud-Santoni (nicoo)
  * Pierre-Elliott Bécue (peb)
  * Stephen Gelman (ssgelm)
  * Daniel Echeverry (epsilon)
  * Dmitry Bogatov (kaction)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos últimos dois meses:

  * Sagar Ippalpalli
  * Kurt Kremitzki
  * Michal Arbet
  * Peter Wienemann
  * Alexis Bienvenüe
  * Gard Spreemann

Parabéns a todos!

