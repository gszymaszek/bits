Title: Debian effectue un don pour soutenir la défense de GNOME attaqué sur les brevets
Slug: debian-donates-support-gnome-patent-defense
Date: 2019-10-28 17:00
Author: Sam Hartman
Tags: debian, gnome, patent trolls, fundraising, donation
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

Aujourd'hui, le projet Debian s'engage à [donner] 5 000 $ à la
fondation GNOME en soutien à sa défense à venir sur les brevets. Le
23 octobre, nous [avons écrit pour exprimer notre soutien à GNOME] dans une
affaire qui touche l'ensemble de la communauté du logiciel libre.
Aujourd'hui nous rendons ce soutien tangible.

« *Cela dépasse GNOME,* » a déclaré Sam Hartman le chef du projet Debian.
« *En nous regroupant et en démontrant que l'ensemble de la communauté du
logiciel libre est derrière GNOME, nous pouvons envoyer un message fort aux
personnes morales sans activité (les chasseurs de brevets). Si vous attaquez
qui que ce soit dans la communauté du logiciel libre, vous nous attaquez
tous. Nous nous battrons et nous lutterons pour invalider vos brevets. Pour
nous, c'est plus qu’un problème d'argent. Il s'agit de la liberté de
construire et distribuer nos logiciels.* »

« *Nous sommes infiniment reconnaissants à Debian pour cette généreuse donation,
et aussi pour son soutien,* » a déclaré Neil McGovern, directeur exécutif de
la fondation GNOME. « *Il est encourageant de voir que quand le logiciel
libre est attaqué de cette manière, nous formons tous un front uni.*»

Si GNOME a besoin de plus de moyens plus tard pour sa défense, Debian sera
là pour soutenir la fondation GNOME. Nous encourageons les personnes et les
organismes à se joindre à nous et rester fermes contre les chasseurs de
brevets.

[avons écrit pour exprimer notre soutien à GNOME]: https://bits.debian.org/2019/10/gnome-foundation-defense-patent-troll-fr.html
[donner]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
