Title: The Debian Project stands with the GNOME Foundation in defense against patent trolls
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Status: published

In 2012, the Debian Project published our [Position on Software Patents],
stating the threat that patents pose to Free Software.

The GNOME Foundation has announced recently that they are fighting a lawsuit
alleging that Shotwell, a free and Open Source personal photo manager,
infringes a patent.

The Debian Project firmly stands with the GNOME Foundation in their efforts to show
the world that we in the Free Software communities will vigorously defend ourselves
against any abuses of the patent system.

Please read [this blog post about GNOME's defense against this patent troll]
and consider making a donation to the [GNOME Patent Troll Defense Fund].

[Position on Software Patents]: https://www.debian.org/legal/patent
[this blog post about GNOME's defense against this patent troll]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[GNOME Patent Troll Defense Fund]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
