Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2019)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Jean-Baptiste Favre (jbfavre)
  * Andrius Merkys (merkys)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Christian Ehrhardt
  * Aniol Marti
  * Utkarsh Gupta
  * Nicolas Schier
  * Stewart Ferguson
  * Hilmar Preusse

Grattis!

