Title: 新的 Debian 开发者和维护者 (2019年9月 至 年10月)
Slug: new-developers-2019-10
Date: 2019-11-26 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Teus Benschop (teusbenschop)
  * Nick Morrott (nickm)
  * Ondřej Kobližek (kobla)
  * Clément Hermann (nodens)
  * Gordon Ball (chronitis)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Nikos Tsipinakis
  * Joan Lledó
  * Baptiste Beauplat
  * Jianfeng Li

祝贺他们！

