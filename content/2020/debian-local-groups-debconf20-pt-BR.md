Title: Grupos locais Debian na DebConf20 e mais além
Slug: debian-local-groups-debconf20
Date: 2020-09-16 19:15
Author: Francisco M. Neto and Laura Arjona Reina
Translator: Emerson dos Santos Queiroz
Lang: pt-BR
Tags:
Status: published

Existem vários grupos locais Debian grandes e de muito sucesso
(Debian França, Debian Brasil and Debian Taiwan, só para citar alguns),
mas o que podemos fazer para ajudar com apoio os próximos grupos locais ou ajudar
a despertar o interesse em mais partes do mundo?

Houve uma sessão sobre
[equipes locais Debian na Debconf20](https://debconf20.debconf.org/talks/50-local-teams/)
e gerou um pouco de discussão construtiva durante a transmissão ao vivo
(gravação disponível em
[https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/)),
na [sessão do Etherpad](https://pad.online.debconf.org/p/50-local-teams)
e no canal do IRC (#debian-localgroups). Este artigo é uma tentativa de resumir
os pontos-chave que foram levantados durante essa discussão, bem como os planos
para as ações futuras de suporte a novos ou existentes grupos locais Debian e a
possibilidade de formar uma equipe de apoio a grupos locais.

## Situação pandêmica

Durante uma pandemia, pode parecer estranho discutir reuniões offline, mas isso
é um bom momento para planejar as coisas para o futuro. Ao mesmo tempo, a atual
situação torna mais importante do que antes incentivar a interação local.

## Argumentação para grupos locais

O Debian pode parecer assustador para quem está de fora. Já ter uma conexão
com o Debian - especialmente com pessoas diretamente envolvidas nele -
parece ser a maneira pela qual a maioria dos(as) contribuidores(as) chega.
Mas se não houver uma conexão, não é tão fácil; grupos locais facilitam isso
melhorando a rede de relacionamentos.

Os grupos locais são incrivelmente importantes para o sucesso do Debian
já que eles frequentemente ajudam com traduções, nos tornando mais
diversificados, com suporte, organizando sprints locais de caça a bugs,
formando uma equipe DebConf local junto com MiniDebConfs, conseguindo
patrocinadores(as) para o projeto e muito mais.

A existência de grupos locais também facilitaria o acesso a brinds como
adesivos e canecas, já que nem sempre as pessoas têm tempo para lidar com o
processo de encontrar um(a) fornecedor(a) para realmente conseguir fazê-los.
A atividade de grupos locais pode facilitar isso, organizando a logística
relacionada.

## Como lidar com grupos locais, como definir um grupo local

O Debian reúne as informações sobre grupos locais em sua
[página wiki de grupos locais](https://wiki.debian.org/LocalGroups) (e subpáginas).
Outras organizações também têm seus próprios esquemas, alguns deles apresentando
um mapa, blogs ou regras claras sobre o que constitui um grupo local. No caso do
Debian não há um conjunto predefinido de "regras", mesmo sobre o nome do grupo.
Isso é perfeitamente normal, presumimos que certos grupos locais podem ser bem
pequenos, ou temporários (criado em torno de um certo tempo quando planejam
várias atividades, e depois fica quieto). No entanto, a forma como os
grupos são nomeados e como eles são listado na página wiki define expectativas
em relação a quais tipos de atividades eles desenvolvem.

Por esta razão, encorajamos todos os grupos locais do Debian a revisar suas
entradas na [wiki Debian](https://wiki.debian.org/LocalGroups), mantenha-o
atualizado (por exemplo, adicione uma linha "Status: Ativo (2020)), e
encorajamos grupos informais de contribuidores(as) do Debian que de alguma forma
"se encontram", para criar um novo cadastro na página wiki também.

## O que o Debian pode fazer para apoiar grupos locais

Ter um banco de dados centralizado de grupos é bom (se atualizado), mas não o
suficiente. Exploraremos outras formas de propagação e aumento da visibilidade,
como organizar a logística de impressão/envio de brindes e facilitar o acesso
ao financiamento de eventos relacionados ao Debian.

## Continuação de esforços

Os esforços devem continuar em relação aos grupos locais.
As reuniões regulares acontecem a cada duas ou três semanas;
pessoas interessadas são encorajadas a explorar algumas outras palestras
relevantes da DebConf20
([Introducing Debian Brasil](https://debconf20.debconf.org/talks/105-introducing-debian-brasil/),
[Debian Academy: Another way to share knowledge about Debian](https://debconf20.debconf.org/talks/30-debian-academy-another-way-to-share-knowledge-about-debian/),
[An Experience creating a local community on a small town](https://debconf20.debconf.org/talks/55-an-experience-creating-a-local-community-in-a-small-town/)),
sites web como [Debian flyers](https://debian.pages.debian.net/debian-flyers/)
(incluindo outro material impresso como cubo, adesivos),
visite a [seção de eventos do site web do Debian](https://www.debian.org/events/)
e a página wiki [Debian Locations](https://wiki.debian.org/DebianLocations),
e participe do canal IRC #debian-localgroups no OFTC.