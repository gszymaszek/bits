Title: The Debian web updates its homepage and prepares for a major renewal
Slug: debian-web-new-homepage
Date: 2020-12-17 13:50
Author: Laura Arjona Reina
Artist: 
Tags: web
Status: published

Today, the [Debian website](https://www.debian.org) displays a new homepage. 
Since the most recent web team [sprint](https://bits.debian.org/2019/04/debian-web-team-sprint-2019.html)
in March 2019, we have been working on renewing the structure, content, layout
and scripts that build the site.
There has been work mainly in two areas: removing or updating obsolete content,
and creating a new homepage which is more attractive to newcomers,
and which also highlights the social aspect of the Debian project
in addition to the operating system we develop.

[![Debian website: part of the old homepage (back) and the new one (front)](|static|/images/debian_web_old_new.png)](https://www.debian.org)

Although this took longer than we would have liked,
and we don't consider this new homepage final,
we think it's a good first step towards a much better web site.

The web team will continue to work on restructuring the Debian website.
We would like to appeal to the community for help, and are also considering
external assistance, since we're a small group, whose members are also involved
in other Debian teams. Some of the next steps we expect to walk are
improve the CSS, icons, and layout in general, and review of the content,
to have a better structure.

If you would like to help, contact us. You can reply to the version of this
article (with some more details)
[published in our public mailing list](https://lists.debian.org/debian-www/2020/12/msg00057.html)
or chat with us in the #debian-www IRC channel (at irc.debian.org).
