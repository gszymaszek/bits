Title: Let's celebrate DebianDay 2020 around the world
Date: 2020-07-22 15:30
Tags: debian, birthday
Slug: lets-celebrate-debianday-2020-around-the-world
Author: Paulo Henrique de Lima Santana (phls)
Lang: en
Status: published

We encourage our community to celebrate around the world the 27th Debian 
anniversary with organized [DebianDay](https://wiki.debian.org/DebianDay) events. This year due to the COVID-19 
pandemic we cannot organize in-person events, so we ask instead that contributors, 
developers, teams, groups, maintainers, and users promote The Debian Project and Debian
activities online on August 16th (and/or 15th).

Communities can organize a full schedule of online activities throughout the day.
These activities can include talks, workshops, active participation with 
contributions such as translations assistance or editing, debates, BoFs, and
all of this in your local language using tools such as [Jitsi](https://meet.jit.si) for capturing 
audio and video from presenters for later streaming to YouTube.

If you are not aware of any local community organizing a full event or you don't
want to join one, you can solo design your own activity using [OBS](https://obsproject.com) and
stream it to YouTube. You can watch an OBS tutorial [here](https://peertube.debian.social/videos/watch/7f41c0e7-66cc-4234-b929-6b3219d95c14).

Don't forget to record your activity as it will be a nice idea to upload it
to [Peertube](https://peertube.debian.social) later.

Please add your event/activity on the [DebianDay wiki page](https://wiki.debian.org/DebianDay/2020) and let us know about
and advertise it on [Debian micronews](https://micronews.debian.org). To share it, you have several options:

* Follow the steps listed [here](https://micronews.debian.org/pages/contribute.html) for Debian Developers.
* Contact us using IRC in channel `#debian-publicity` on the OFTC network, and ask us there.
* Send a mail to [debian-publicity@lists.debian.org](https://lists.debian.org/debian-publicity/)
and ask for your item to be included in micronews. This is a publicly archived list.

PS: DebConf20 online is coming! It will be held from August 23rd to 29th, 2020.
[Registration is already open](https://debconf20.debconf.org/news/2020-07-12-registration-is-open/).

