Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (março e abril de 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram desenvolvedores(as) Debian nos últimos dois meses:

  * Paride Legovini (paride)
  * Ana Custura (acute)
  * Felix Lechner (lechner)

Os(As) seguintes colaboradores(as) do projeto se tornaram mantenedores(as) Debian nos últimos dois meses:

  * Sven Geuer
  * Håvard Flaget Aasen

Parabéns a todos(as)!

