Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng ba và tư 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Paride Legovini (paride)
  * Ana Custura (acute)
  * Felix Lechner (lechner)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Sven Geuer
  * Håvard Flaget Aasen

Xin chúc mừng!

