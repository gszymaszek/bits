Title: Nya Debianutvecklare och Debian Maintainers (September och Oktober 2020)
Slug: new-developers-2020-10
Date: 2020-11-16 20:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Benda XU (orv)
  * Joseph Nahmias (jello)
  * Marcos Fouces (marcos)
  * Hayashi Kentaro (kenhys)
  * James Valleroy (jvalleroy)
  * Helge Deller (deller)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Ricardo Ribalda Delgado
  * Pierre Gruet
  * Henry-Nicolas Tourneur
  * Aloïs Micard
  * Jérôme Lebleu
  * Nis Martensen
  * Stephan Lachnit
  * Felix Salfelder
  * Aleksey Kravchenko
  * Étienne Mollier

Grattis!

