Title: Élection du chef du projet Debian 2020, félicitations Jonathan Carter!
Date: 2020-04-19 14:25
Tags: dpl
Slug: results-dpl-elections-2020
Author: Ana Guerrero López
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

L'élection du chef du projet Debian vient de s'achever et le vainqueur est
Jonathan Carter.

Son mandat de chef du projet débute le 21 avril et
s'achèvera le 20 avril 2021.

Sur un total de 1011 développeurs, 339 ont voté avec la [méthode Condorcet](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Condorcet).
Davantage d'informations sur le résultat sont disponibles sur la [page de l'élection 2020 du chef du projet Debian](https://www.debian.org/vote/2020/vote_001).

Merci beaucoup à [Jonathan Carter](https://www.debian.org/vote/2020/platforms/jcc),
[Sruthi Chandran](https://www.debian.org/vote/2020/platforms/srud) et
[Brian Gupta](https://www.debian.org/vote/2020/platforms/bgupta) pour s'être présentés.

Et un remerciement particulier pour Sam Hartman pour son rôle de chef du projet
Debian des douze derniers mois !
