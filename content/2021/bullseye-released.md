Title: Debian 11 "bullseye" has been released!
Date: 2021-08-14 23:30
Tags: bullseye
Slug: bullseye-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Artist: Juliette Taka
Status: published

[![Alt Bullseye has been released](|static|/images/banner_bullseye.png)](https://deb.li/bullseye)

We're happy to announce the release of Debian 11, codenamed *bullseye*!

**Want to install it?**
Choose your favourite [installation media](https://www.debian.org/distrib/)
and read the [installation manual](https://www.debian.org/releases/bullseye/installmanual).
You can also use an official cloud image directly on your cloud provider,
or try Debian prior to installing it using our "live" images.

**Already a happy Debian user and you only want to upgrade?**
You can easily upgrade from your current Debian 10 "buster" installation;
please read the [release notes](https://www.debian.org/releases/bullseye/releasenotes).

**Do you want to celebrate the release?**
We provide some [bullseye artwork](https://wiki.debian.org/DebianArt/Themes/Homeworld) that you
can share or use as base for your own creations. Follow the conversation about bullseye in social media via the #ReleasingDebianBullseye
and #Debian11Bullseye hashtags or join an in-person or online [Release Party](https://wiki.debian.org/ReleasePartyBullseye)!
