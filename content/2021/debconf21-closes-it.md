Title: Si è conclusa la DebConf21 online
Date: 2021-09-09 15:00
Tags: debconf21, announce, debconf
Slug: debconf21-closes
Author: Laura Arjona Reina and Donald Norwood
Artist: Judit Foglszinger
Lang: it
Translator: Giuseppe Sacco
Status: published

[![Foto di gruppo della DebConf21 - clicca per ingrandire](|static|/images/debconf21_group_small.jpg)](https://wiki.debian.org/DebConf/21/GroupPhoto)
Sabato 28 agosto 2021 l'annuale conferenza degli sviluppatori Debian
è arrivata a conclusione.

DebConf21 si è tenuta online per la seconda volta a causa della pandemia
del coronavirus (COVID-19).

Tutte le sessioni sono state trasmesse permettendo varie modalità di partecipazione:
tramite messaggi IRC, documenti per la collaborazione online, stanze per la video
conferenza.

Con 740 partecipanti registrati da oltre 15 nazioni, un totale di 70 sessioni
tra presentazioni e discussioni, incontri «Birds of a Feather (BoF)»
(incontri su temi specifici) e altre iniziative,
[DebConf21](https://debconf21.debconf.org) è stata un grande successo.

La struttura consolidata durante i precedenti eventi online che
utilizzava Jitsi, OBS, Voctomix, SReview, nginx, Etherpad, un
frontend web per voctomix è stata ulteriormente migliorata e usata
con successo per DebConf21.
Tutti i componenti della infrastruttura video sono software libero e sono
stati configurati come mostrato nel «repository» pubblico del gruppo video
che utilizza [ansible](https://salsa.debian.org/debconf-video-team/ansible).


Il [calendario](https://debconf21.debconf.org/schedule/) DebConf21 ha
incluso una pletora di eventi raggruppati in alcuni percorsi:

  * introduzione a software libero &amp; Debian,
  * pacchettizzazione, norme e infrastruttura Debian,
  * amministrazione di sistema, automazione e orchestrazione,
  * cloud e container,
  * sicurezza,
  * comunità, diversità, incontri locali e contesti sociali,
  * internazionalizzazione, localizzazione e accessibilità,
  * sistemi «embedded» &amp; kernel,
  * Debian Blends e distribuzioni derivate da Debian,
  * Debian nelle arti &amp; scienze
  * e altro.

Le presentazioni sono state trasmesse in due stanze, e varie di queste attività
sono state tenute in lingue diverse: telugu, portoghese, malese, kannada,
hindi, marathi e inglese, permettendo a audience diverse di partecipare agevolmente.

Negli intermezzi tra le presentazioni, la trasmissione ha mostrato gli sponsor,
in ciclo, oltre a video aggiuntivi che includevano foto di precedenti DebConf,
fatti divertenti su Debian e brevi interventi inviati dai partecipanti per comunicare
con i propri amici di Debian.

Il gruppo della pubblicità di Debian ha gestito la copertura mediatica per
incoraggiare la partecipazione con micro annunci per i diversi eventi. Il gruppo
DebConf ha anche fornito varie
[opzioni per seguire il programma su dispositivi mobili](https://debconf21.debconf.org/schedule/mobile/).


Per quelli che non hanno potuto partecipare, gran parte delle presentazioni e delle sessioni
sono già disponibili sul sito web 
[degli incontri Debian](https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/),
mentre quelle che mancano saranno disponibili nei prossimi giorni.

Il sito web [DebConf21](https://debconf21.debconf.org/)
rimarrà attivo come archivio e continuerà ad offrire collegamenti
alle presentazioni e ai video di eventi.

L'anno prossimo, [DebConf22](https://wiki.debian.org/DebConf/22) si terrà
a Prizren, in Cossovo, in luglio 2022.


DebConf offre un ambiente sicuro e accogliente a tutti i partecipanti.
Durante la conferenza vari gruppi (segreteria, accoglienza e gruppo della comunità)
sono stati disponibili per l'aiuto in modo che i partecipanti potessero avere
la migliore esperienza possibile, e hanno cercato di trovare soluzioni ad ogni
problema sollevato. Vedere la
[pagina web del codice di condotta sul sito web DebConf21](https://debconf21.debconf.org/about/coc/) per maggiori informazioni.


Debian ringrazia per la partecipazione numerosi [sponsor](https://debconf21.debconf.org/sponsors/)
che hanno supportato DebConf21, in particolare gli sponsor platino:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Roche**](https://code4life.roche.com),
[**Amazon Web Services (AWS)**](https://aws.amazon.com/)
e [**Google**](https://google.com/).


### Su Debian

Il Progetto Debian è stato fondato nel 1993 con il fine di essere un
progetto di una comunità veramente libera. Da lì, il progetto è cresciuto
fino a diventare uno dei più vasti e influenti progetti open source.
Migliaia di volontari da tutto il mondo collaborano al fine di creare e di
mantenere il software Debian. Disponibile in 70 lingue e con una vasta gamma
di architetture supportate, Debian si definisce il _sistema operativo universale_.


### Su DebConf

DebConf è la conferenza degli sviluppatori del progetto Debian. Oltre ad un 
programma fitto di interventi tecnici, sociali e relativi alla policy, DebConf 
offre agli sviluppatori, ai collaboratori e alle altre persone interessate 
un'opportunità per incontrarsi di persona e per lavorare insieme a più stretto 
contatto. È stata organizzata ogni anno, a partire dal 2000, in varie sedi come
Scozia, Argentina e Bosnia-Erzegovina. Maggiori informazioni su DebConf sono
disponibili all'indirizzo [https://debconf.org/](https://debconf.org).


### Su Lenovo

In qualità di leader tecnologico globale e produttore di una gran quantità di
prodotti connessi, inclusi telefoni cellulari, tablet, computer personali e da
lavoro, oltre che dispositivi AR/VR e soluzioni per casa/ufficio e datacentre,
[**Lenovo**](https://www.lenovo.com) sa bene quanto siano critici i sistemi
e le piattaforme aperte nel mondo connesso.


### Su Infomaniak

[**Infomaniak**](https://www.infomaniak.com) è la più grande società
di «hosting» web svizzera che offre anche servizi per il backup e lo «storage»,
per supportare organizzatori di eventi, dirette streaming e video a richiesta.
Posside interamente i propri datacentre e tutti gli elementi importanti per il
funzionamento dei servizi e dei prodotti forniti dall'azienda (sia
software che hardware).

### Su Roche

[**Roche**](https://code4life.roche.com/) è una delle maggiori società di
ricerca e fornitura del settore farmaceutico legata alla salute.
Più di 100.000 dipendenti in tutto il mondo lavorano per risolvere alcune delle
grandi sfide dell'umanità usando scienza e tecnologia. Roche è fortemente
legata a progetti di ricerca e collaborazione con finanziamenti pubblici con
altri partner industriali e accademici, inoltre supporta DebConf dal 2017.

### Su Amazon Web Services (AWS)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) è
una delle maggiori piattaforme cloud più complete e maggiormente usate, offre
più di 175 servizi da datacentre globali (divisi in 77 zone distruibite in
24 regioni geografiche).
I clienti AWS includono le startup a grande crescita, le grandi imprese e
le maggiori agenzie governative.

### Su Google

[**Google**](https://google.com/) è una delle più grandi società
tecnologiche al mondo. Offre un grande porafoglio di servizi legati ad Internet
e di prodotti tecnologici quali la pubblicità online, la ricerca, supporto alle
applicazioni cloud, software e hardware.


Google supporta Debian sponsorizzando DebConf da oltre 10 anni ed è anche
un partner Debian che sponsorizza parte dell'infrastruttura per
l'integrazione continua di [Salsa](https://salsa.debian.org)
all'interno della piattaforma cloud di Google.

### Per maggiori informazioni

Per ulteriori informazioni visitare le pagine web DebConf21 su
[https://debconf21.debconf.org/](https://debconf21.debconf.org/)
o inviare un email a <press@debian.org>.
