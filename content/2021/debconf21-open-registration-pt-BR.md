Title: Já estão abertas as inscrições para a DebConf21 online
Date: 2021-06-08 10:00
Tags: debconf, debconf21
Slug: debconf21-open-registration
Author: Stefano Rivera
Translator: Paulo Henrique de Lima Santana (phls)
Lang: pt-BR
Status: published

![DebConf21 banner](|static|/images/dc21-logo-horizontal-diversity.png)

A equipe da DebConf tem o prazer de anunciar que as inscrições para a DebConf21
online já estão abertas.

A 21ª Conferência Debian será realizada online, devido ao COVID-19, de 22 a 29
de agosto de 2021.
Teremos também uma DebCamp de 15 a 21 de agosto de 2021 (precedendo o DebConf).

Para se inscrever na DebConf21, por favor acesse nosso website em
[https://debconf21.debconf.org/register](https://debconf21.debconf.org/register)

Lembrete: a criação de uma conta no site não inscreve você na DebConf21.
Existe um outro formulário de inscrição para o evento que deve ser preenchido
após o login clicando do link "Register".

A participação na DebConf21 está condicionada ao seu respeito ao nosso
[Código de Conduta](https://debconf21.debconf.org/about/coc).
Exigimos que você o leia, compreenda e cumpra este código.

Algumas observações sobre o processo de inscrição:

- Precisamos saber a localização dos(as) participantes para planejar melhor a
  programação de acordo com os fusos horários. Por favor, certifique-se de
  preencher corretamente o campo "Country I call home" no formulário de
  inscrição. Ter esses dados é especialmente importante para as pessoas que
  enviaram palestras, mas também para outros(as) participantes.

- Estamos oferecendo uma quantidade limitada de apoio financeiro para aquelas
  pessoas que precisam de ajuda para participar. Consulte a página
  [bursaries](https://debconf21.debconf.org/about/bursaries/) no site para obter mais informações.

Qualquer dúvida sobre a inscrição deve ser enviada para
<registration@debconf.org>.

Nos vamos online!

A DebConf não seria possível sem o apoio generoso de todos os nossos
patrocinadores, especialmente nossos patrocinadores Platinum
[**Lenovo**](https://www.lenovo.com) e [Infomaniak](https://www.infomaniak.com),
e nosso patrocinador Gold [**Matanel Foundation**](http://www.matanel.org/).
