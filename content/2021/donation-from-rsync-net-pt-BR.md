Title: Doação de rsync.net para o projeto Debian e benefícios para membros do Debian
Slug: donation-from-rsync-net
Date: 2021-05-28 13:45
Author: Donald Norwood and Laura Arjona Reina
Translator: Carlos Henrique Lima Melara, Adriano Rafael Gomes
Lang: pt-BR
Tags: donation, cloud
Status: published

Nós estamos felizes em anunciar que a empresa de backup externo (offsite)
e armazenamento em nuvem [**rsync.net**](https://www.rsync.net/) doou
generosamente muitos Terabytes de espaço de armazenamento para o projeto
Debian! Essa nova mídia de armazenamento será usada para fazer o backup
da [instância Peertube do Debian](https://peertube.debian.social/).

Além dessa oferta generosa, rsync.net também está [disponibilizando](https://www.rsync.net/debian.html)
uma conta gratuita para sempre com 500 GB de armazenamento para cada
desenvolvedor(a) Debian.

A rsync.net é uma empresa dedicada a backup externo (offsite). Desde 2001,
eles proporcionam a seus clientes um sistema de arquivos UNIX seguro e acessível
à maioria das aplicações SSH e SFTP. A infraestrutura da rsync.net está
espalhada por vários continentes com um núcleo de rede IPv6 e um sistema
de arquivos ZFS redundante de forma a assegurar a segurança e integridade
dos dados armazenados por seus clientes.

O projeto Debian agradece a **rsync.net** por sua generosidade e seu apoio.
