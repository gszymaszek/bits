Title: Donation from rsync.net to the Debian Project and benefits for Debian members
Slug: donation-from-rsync-net
Date: 2021-05-28 13:45
Author: Donald Norwood and Laura Arjona Reina
Tags: donation, cloud
Status: published

We are pleased to announce that offsite backup and cloud storage company [**rsync.net**](https://www.rsync.net/) has generously donated several Terabytes of storage space to the Debian Project! 
This new storage medium will be used to backup our [Debian Peertube instance](https://peertube.debian.social/).
 
In addition to this bountiful offer, rsync.net is also [providing](https://www.rsync.net/debian.html) a free-forever 500 GB account to every Debian Developer.

rsync.net is a dedicated offsite backup company.  Since 2001, they have provided customers with a secure UNIX filesystem accessible with most SSH/SFTP applications. rsync.net’s  infrastructure is spread across multiple continents with a core IPv6 network and a ZFS redundant file-system assuring customer data is kept securely with integrity.

The Debian Project thanks **rsync.net** for their generosity and support.
