Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (maio e junho de 2021)
Slug: new-developers-2021-06
Date: 2021-07-22 15:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * Timo Röhling (roehling)
  * Patrick Franz (deltaone)
  * Christian Ehrhardt (paelzer)
  * Fabio Augusto De Muzio Tobich (ftobich)
  * Taowa (taowa)
  * Félix Sipma (felix)
  * Étienne Mollier (emollier)
  * Daniel Swarbrick (dswarbrick)
  * Hanno Wagner (wagner)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Evangelos Ribeiro Tzaras
  * Hugh McMaster

Parabéns a todos!

