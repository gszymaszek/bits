Title: DebConf22 välkomnar sina sponsorer!
Slug: debconf22-welcomes-sponsors
Date: 2022-07-18 09:00
Author: The Debian Publicity Team
Tags: debconf, debconf22, sponsors
Lang: sv
Translator: Andreas Rönnquist
Image: /images/debconf22-official-logo.png
Status: published

[DebConf22](https://debconf22.debconf.org) äger rum i Prizren, Kosovo,
från den 17:de till den 24:de juli, 2022.
Det är den 23:e upplagan av Debiankonferensen och organisatörer jobbar hårt på
att skapa ytterligare ett intressant och fruktbart evenemang för deltagarna.

Vi skulle vilja varmt välkomna sponsorerna av DebConf22, och
introducera dem för er.

Vi har fyra Platinasponsorer.

Vår första Platinasponsor är [**Lenovo**](https://www.lenovo.com).
Som global ledare inom teknologi och tillverkare av en bred portfölj av
uppkopplade produkter inklusive mobiltelefoner, surfplattor, datorer och
arbetsstationer så väl som AR/VR-enheter, lösningar för smarta hem/smarta
kontor och datacenter, förstår Lenovo hur kritiska öppna system och
plattformar är för en uppkopplad värld.

[**Infomaniak**](https://www.infomaniak.com/en) är vår andra platinasponsor.
Informaniak är Schweiz största webbhotell,
som även erbjuder backup- och lagringstjänster, lösningar för organisatörer
av evenemang, live-strömning och video on demand-tjänster.
Företaget äger sina datahallar och alla element som är kritiska
för driften av tjänsterna och produkterna som tillhandahålls
(både mjukvara och hårdvara).

The [**ITP Prizren**](https://itp-prizren.com) är vår tredje platinasponsor.
ITP Prizren har för avsikt att vara ett föränderligt och stärkande
element inom Information och Kommunikationsteknologi, agro-food och
kreativa industrier, genom skapande och förvaltning av en gynnsam miljö
och effektiva tjänster för små och medelstora företag, utnyttja olika typer
av innovationer som kan bidra till Kosovo för att förbättra utvecklingsnivån
inom industri och forskning, vilket ger fördelar till ekonomin och landet i
stort.

[**Google**](https://google.com) är vår fjärde platinasponsor.
Google är en av världens största teknologiföretag, som tillhandahåller
en bredd av Internetrelaterade tjänster och produkter så som
onlineannonseringsteknik, sökning, cloud computing, mjukvara och hårdvara.
Google har stöttat Debian genom att sponsra DebConf i mer än tio år och är
även Debianpartner som sponsrar delar av [Salsa](https://salsa.debian.org)s
kontinuerliga integrationsinfrastruktur inom Google Cloud Platform.

Våra Guldsponsorer är:

[**Roche**](https://code4life.roche.com),
en stor internationell läkemedelsleverantör och forskningsföretag dedikerade
till personlig sjukvård.

[**Microsoft**](https://www.microsoft.com),
möjliggör digital transformation för en tid präglad av ett intelligent moln och
en intelligent spets. Dess uppdrag är att stärka varje person och varje
organisation på planeten så att dom uppnår mer.

[**Ipko Telecommunications**](https://www.ipko.com),
tillhandahåller telekommunikationstjänster och är den första och den
mest dominanta mobiloperatören som erbjuder snabbt mobilt internet – 3G och 
4G-nätverk i Kosovo.

[**Ubuntu**](https://www.canonical.com),
operativsystemet som levereras av Canonical.

[**U.S. Agency for International Development**](https://www.usaid.gov/kosovo),
leder internationell utveckling och humanitära insatser för att rädda liv,
minska fattigdom, stärka demokratiskt styre och hjälpa människor till framsteg
bortom assistans.

Våra silversponsorer är:

[**Pexip**](https://www.pexip.com), är videokommunikationsplattformen som löser
stora organisationers behov.
[**Deepin**](https://deepin.org/) är ett kinesiskt kommersiellt företag som
fokus på utveckling och service av Linux-baserade operativsystem.
[**Hudson River Trading**](https://www.hudsonrivertrading.com),
ett företag som forskar och utvecklar automatiserade handelsalgoritmer som
använder avancerade matematiska tekniker.
[**Amazon Web Services (AWS)**](https://aws.amazon.com),
är ett av världens mest omfattande och brett använda molnplattformar,
erbjuder över 175 fullt utrustade tjänster från datacenter globalt.
[**Bern University of Applied Sciences**](https://www.bfh.ch)
med nästan [7,800](https://www.bfh.ch/en/bfh/facts_figures.html) studenter,
placerat i Schweiziska huvudstaden.
[**credativ**](http://www.credativ.de),
ett tjänstorienterat företag med fokus på mjukvara med öppen källkod, samt även 
en [parter för Debianutveckling](https://www.debian.org/partners).
[**Collabora**](https://www.collabora.com),
ett globalt konsultföretag som levererar mjukvarulösningar med öppen källkod
till den kommersiella världen.
[**Arm**](https://www.arm.com): med världens bästa SoC Design Portfolio,
har Armdrivna lösningar stött innovationen i mer än 30 år och har hittills
distribuerats i över 225 miljarder chips.
[**GitLab**](https://about.gitlab.com/solutions/open-source),
en end-to-end mjukvaruutvecklingsplattfor baserad på öppen källkod med
inbyggd versionshantering, felspårningssystem, kodgranskning, CI/CD, med mera.
[**Two Sigma**](https://www.twosigma.com),
rigorösa undersökningar, dataanalys och uppfinningar för att lösa de
svåraste utmaningarna inom finansiella tjänster.
[**Starlabs**](https://www.starlabs.dev), bygger mjukvaruupplevelser och
fokuserar på at bygga team som levererar kreativa tekniska lösningar för våra
kunder.
[**Solaborate**](https://www.solaborate.com), har världens mest
integrerade och kraftfulla virtuella vårdleveransplattform.
[**Civil Infrastructure Platform**](https://www.cip-project.org),
ett samarbetsprojekt som drivs av Linux Foundation, som etablerar ett 
"baslager" med programvara baserad på öppen källkod av industriell kvalitet.
[**Matanel Foundation**](http://www.matanel.org), verkar i Israel,
eftersom dess första angelägenhet är att bevara sammanhållningen i ett samhälle
och en nation som plågas av splittring.

Bronssponsorer:

[**bevuta IT**](https://www.bevuta.com/en),
[**Kutia**](https://kutia.net),
[**Univention**](https://www.univention.com),
[**Freexian**](https://www.freexian.com/services/debian-lts.html).

Och slutligen, våra sponsorer med supporternivå:

[**Altus Metrum**](https://altusmetrum.org),
[**Linux Professional Institute**](https://www.lpi.org),
[**Olimex**](https://www.olimex.com),
[**Trembelat**](https://trembelat.com),
[**Makerspace IC Prizren**](https://makerspaceprizren.com),
[**Cloud68.co**](https://cloud68.co),
[**Gandi.net**](https://www.gandi.net/en-US),
[**ISG.EE**](https://isg.ee.ethz.ch),
[**IPKO Foundation**](https://ipkofoundation.org),
[**The Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH**](https://www.giz.de/en/worldwide/298.html).


Tack till alla våra sponsorer för deras stöd!
Deras bidrag gör det möjligt för ett större antal
Debianbidragslämnare från hela världen att arbeta tillsammans,
hjälpa varandra och lära sig från varandra under DebConf22.

![DebConf22 logo](|static|/images/debconf22-official-logo.png)
