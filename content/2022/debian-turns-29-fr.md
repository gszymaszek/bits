Title: Debian fête ses 29 ans !
Date: 2022-08-16 13:00
Tags: debian, birthday
Slug: debian-turns-29
Author: Laura Arjona Reina
Artist: Juliette Taka
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

[![Alt Joyeux anniversaire Debian par Juliette Taka - cliquez pour agrandir](|static|/images/happy_birthday_Debian_29_600x600.png)](https://bits.debian.org/images/happy_birthday_Debian_29.png)

Aujourd'hui c'est le vingt-neuvième anniversaire de Debian. Nous
[avons proposé](https://bits.debian.org/2022/08/debianday2022-call-for-celebration.html)
récemment quelques idées pour célébrer le DebianDay, et plusieurs événements
ont été [organisés](https:/ /wiki.debian.org/DebianDay/2022) dans plus de
quatorze endroits. Vous pouvez vous joindre à la fête ou organiser quelque chose
vous aussi !

Aujourd'hui, c'est aussi l'occasion pour vous de débuter ou reprendre vos
contributions à Debian. Par exemple, vous pouvez jeter un œil à notre liste
[d'équipes Debian](https://wiki.debian.org/Teams/), installer le
[paquet how-can-i-help](https://packages.debian.org/bullseye/how-can-i-help)
et voir s'il y a un bogue dans un logiciel que vous pourriez aider à corriger,
commencer à concevoir un projet de thème graphique pour la prochaine version de
Debian, contribuer par de petits conseils sur comment installer Debian sur une
machine comme la vôtre dans les pages du
[https://wiki.debian.org/InstallingDebianOn/](https://wiki.debian.org/InstallingDebianOn/)
ou bien encore mettre une [version autonome de Debian](https://www.debian.org/CD/live/) sur
une clé USB et la donner à un de vos proches qui n'a pas encore découvert Debian.

Notre système d'exploitation favori est le résultat de tout le travail que nous
avons réalisé toutes et tous ensemble. Merci à tous ceux qui ont contribué
durant ces 29 années, et joyeux anniversaire Debian !
