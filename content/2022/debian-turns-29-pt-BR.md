Title: Debian tem agora 29!
Date: 2022-08-16 13:00
Tags: debian, birthday
Slug: debian-turns-29
Author: Laura Arjona Reina
Artist: Juliette Taka
Translator: Rafael Fontenelle
Lang: pt-BR
Status: published

[![Alt Feliz aniversário ao Debian por Juliette Taka - clique para aumentar](|static|/images/happy_birthday_Debian_29_600x600.png)](https://bits.debian.org/images/happy_birthday_Debian_29.png)

Hoje é o 29º aniversário do Debian. Recentemente [escrevemos](https://bits.debian.org/2022/08/debianday2022-call-for-celebration.html)
sobre algumas ideias para celebrar o DebianDay,
e vários eventos foram [planejados](https:/ /wiki.debian.org/DebianDay/2022)
em mais de 14 locais. Você pode participar da festa ou organizar algo você também!

Hoje também é uma oportunidade para você iniciar ou retomar suas
contribuições ao Debian. Por exemplo, você pode dar uma olhada em
nossa lista de [equipes do Debian](https://wiki.debian.org/Teams/),
instalar o [pacote how-can-i-help](https://packages.debian.org/bullseye/how-can-i-help)
e veja se há um bug em algum software que você usa que você pode ajudar a corrigir,
comece a projetar sua arte candidata para o próximo lançamento,
contribua com pequenas dicas sobre como instalar o Debian
em suas máquinas em nossas páginas wiki [https://wiki.debian.org/InstallingDebianOn/](https://wiki.debian.org/InstallingDebianOn/)
ou coloque um [Debian live](https://www.debian.org/CD/live/) em um pendrive USB
e entregue-a a uma pessoa próxima a você, que ainda não descobriu o Debian.

Nosso sistema operacional favorito é o resultado de todo o trabalho que fazemos juntos(as).
Obrigado a todos(as) que contribuíram nestes 29 anos, e feliz aniversário ao Debian!
