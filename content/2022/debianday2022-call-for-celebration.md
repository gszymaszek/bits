Title: Debian Day 2022 - call for celebration
Slug: debianday2022-call-for-celebration
Date: 2022-08-08 17:00
Author: The Debian Publicity Team
Tags: debian, birthday
Lang: en
Status: published

Every year on August 16th, the anniversary of the Debian Project takes place.
And several communities around the world celebrate this date by organizing local
meetings in an event called "Debian Day".

So, how about celebrating the 29th anniversary of the Debian Project in 2022 in
your city?

We invite you and your local community to organize Debian Day by hosting an
event with talks, workshops, [bug squashing party](https://wiki.debian.org/BSP),
[OpenPGP keysigning](https://wiki.debian.org/Keysigning), etc.
Or simply holding a meeting between people who like Debian in a
bar/pizzeria/cafeteria/restaurant to celebrate. In other words, any type of
meeting is valid!

But remember that the COVID-19 pandemic is not over yet, so take all necessary
measures to protect attendees.

As the 16th of August falls on a Tuesday, if you think it's better to organize
it during the weekend, no problem. The importance is to celebrate the Debian
Project.

Remember to add your city to the
[Debian Day wiki page](https://wiki.debian.org/DebianDay/2022)

There is a [list of Debian Local Groups](https://wiki.debian.org/LocalGroups)
around the world. If your city is listed, talk to them to organize DebianDay
together.

There is a [list of Debian Local Groups](https://wiki.debian.org/LocalGroups)
around the world. If your city is listed, talk to them to organized the Debian
Day together.

Let's use hashtags #DebianDay #DebianDay2022 on social media.
