Title: 新的 Debian 开发者和维护者 (2021年11月 至 年12月)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Douglas Andrew Torrance (dtorrance)
  * Mark Lee Garrett (lee)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Lukas Matthias Märdian
  * Paulo Roberto Alves de Oliveira
  * Sergio Almeida Cipriano Junior
  * Julien Lamy
  * Kristian Nielsen
  * Jeremy Paul Arnold Sowden
  * Jussi Tapio Pakkanen
  * Marius Gripsgard
  * Martin Budaj
  * Peymaneh
  * Tommi Petteri Höynälänmaa

祝贺他们！

