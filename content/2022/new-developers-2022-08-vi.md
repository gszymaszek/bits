Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng bảy và tám 2022)
Slug: new-developers-2022-08
Date: 2022-09-26 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Sakirnth Nagarasa (sakirnth)
  * Philip Rinn (rinni)
  * Arnaud Rebillout (arnaudr)
  * Marcos Talau (talau)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Xiao Sheng Wen
  * Andrea Pappacoda
  * Robin Jarry
  * Ben Westover
  * Michel Alexandre Salim

Xin chúc mừng!

