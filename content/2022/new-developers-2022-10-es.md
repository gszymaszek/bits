Title: Nuevos desarrolladores y mantenedores de Debian (septiembre y octubre del 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Abraham Raji (abraham)
  * Phil Morrell (emorrp1)
  * Anupa Ann Joseph (anupa)
  * Mathias Gibbens (gibmat)
  * Arun Kumar Pariyar (arun)
  * Tino Didriksen (tinodidriksen)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Gavin Lai
  * Martin Dosch
  * Taavi Väänänen
  * Daichi Fukui
  * Daniel Gröber
  * Vivek K J
  * William Wilson
  * Ruben Pollan

¡Felicidades a todos!

