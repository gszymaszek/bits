Title: नए डेबियन डेवलपर्स और मेंटेनर्स (सितंबर और अक्टूबर 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published


पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

  * Abraham Raji (abraham)
  * Phil Morrell (emorrp1)
  * Anupa Ann Joseph (anupa)
  * Mathias Gibbens (gibmat)
  * Arun Kumar Pariyar (arun)
  * Tino Didriksen (tinodidriksen)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

  * Gavin Lai
  * Martin Dosch
  * Taavi Väänänen
  * Daichi Fukui
  * Daniel Gröber
  * Vivek K J
  * William Wilson
  * Ruben Pollan

इन्हें बधाईयाँ!

