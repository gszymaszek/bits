Title: Nya Debianutvecklare och Debian Maintainers (September och Oktober 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Abraham Raji (abraham)
  * Phil Morrell (emorrp1)
  * Anupa Ann Joseph (anupa)
  * Mathias Gibbens (gibmat)
  * Arun Kumar Pariyar (arun)
  * Tino Didriksen (tinodidriksen)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Gavin Lai
  * Martin Dosch
  * Taavi Väänänen
  * Daichi Fukui
  * Daniel Gröber
  * Vivek K J
  * William Wilson
  * Ruben Pollan

Grattis!

