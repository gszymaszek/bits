Title: Debian 12 „bookworm” został wydany!
Date: 2023-06-10 23:30
Tags: bookworm
Slug: bookworm-released
Author: Ana Guerrero Lopez, Laura Arjona Reina oraz Jean&shy;&#8209;Pierre Giraud
Artist: Juliette Taka
Translator: Grzegorz Szymaszek
Lang: pl
Status: published

[![Baner z&nbsp;logo Debian 12 „bookworm”](|static|/images/banner_bookworm.png)](https://www.debian.org/releases/bookworm/)

Mamy przyjemność ogłosić wydanie Debiana 12 o&nbsp;nazwie kodowej
*<span lang="en">bookworm</span>*!

**Chcesz go zainstalować?**
Wybierz swoje ulubione [medium instalacyjne](https://www.debian.org/distrib/)
i&nbsp;przeczytaj
[podręcznik instalacji](https://www.debian.org/releases/bookworm/installmanual).
Możesz również użyć oficjalnego obrazu chmurowego od Twojego dostawcy chmury lub
wypróbować Debiana przed instalacją używając naszych obrazów
„<span lang="en">live</span>”.

**Jesteś już szczęśliwym użytkownikiem Debiana i&nbsp;chcesz go zaktualizować?**
Możesz łatwo zaktualizować Twoją bieżącą instalację Debiana 11
„<span lang="en">bullseye</span>”; przeczytaj
[informacje o&nbsp;nowym wydaniu](https://www.debian.org/releases/bookworm/releasenotes).

**Chcesz świętować nowe wydanie?**
Udostępniamy
[tematyczne ilustracje](https://wiki.debian.org/DebianArt/Themes/Emerald), które
możesz udostępniać lub wykorzystać jako podstawę do tworzenia własnych
opracowań.
Śledź informacje o&nbsp;„<span lang="en">bookworm</span>ie” w&nbsp;mediach
społecznościowych poprzez hasztagi `#ReleasingDebianBookworm` oraz
`#Debian12Bookworm` lub dołącz osobiście lub zdalnie na
[przyjęcie](https://wiki.debian.org/ReleasePartyBookworm)!
