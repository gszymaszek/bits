Title: Debian 12 "bookworm" has been released!
Date: 2023-06-10 23:30
Tags: bookworm
Slug: bookworm-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Artist: Juliette Taka
Status: published

[![Alt Bookworm has been released](|static|/images/banner_bookworm.png)](https://www.debian.org/releases/bookworm/)

We're happy to announce the release of Debian 12, codenamed *bookworm*!

**Want to install it?**
Choose your favourite [installation media](https://www.debian.org/distrib/)
and read the [installation manual](https://www.debian.org/releases/bookworm/installmanual).
You can also use an official cloud image directly on your cloud provider,
or try Debian prior to installing it using our "live" images.

**Already a happy Debian user and you only want to upgrade?**
You can easily upgrade from your current Debian 11 "bullseye" installation;
please read the [release notes](https://www.debian.org/releases/bookworm/releasenotes).

**Do you want to celebrate the release?**
We provide some [bookworm artwork](https://wiki.debian.org/DebianArt/Themes/Emerald) that you
can share or use as base for your own creations. Follow the conversation about bookworm in social media via the #ReleasingDebianBookworm
and #Debian12Bookworm hashtags or join an in-person or online [Release Party](https://wiki.debian.org/ReleasePartyBookworm)!
