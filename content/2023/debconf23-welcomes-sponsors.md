Title: DebConf23 welcomes its sponsors!
Slug: debconf23-welcomes-sponsors
Date: 2023-09-10 11:00
Author: The Debian Publicity Team
Tags: debconf, debconf23, sponsors
Lang: en
Image: /images/debconf23-official-logo.png
Status: published

![DebConf23 logo](|static|/images/DebConf_2023_Logo.400x375.png)

[DebConf23](https://debconf23.debconf.org/), the 24th edition of the Debian
conference is taking place in Infopark at Kochi, Kerala, India.
Thanks to the hard work of its organizers, it will be, this year as well, an
interesting and fruitful event for attendees.

We would like to warmly welcome the sponsors of DebConf23, and
introduce them to you.

We have three Platinum sponsors.

- Our first Platinum sponsor is [**Infomaniak**](https://www.infomaniak.com/).
  Infomaniak is a key player in the European cloud market and the leading
  developer of Web technologies in Switzerland. It aims to be an independent
  European alternative to the web giants and is committed to an ethical and
  sustainable Web that respects privacy and creates local jobs. Infomaniak
  develops cloud solutions (IaaS, PaaS, VPS), productivity tools for online
  collaboration and video and radio streaming services.

- [**Proxmox**](https://www.proxmox.com/) is our second Platinum sponsor.
  Proxmox develops powerful, yet easy-to-use open-source server software.
  The product portfolio from Proxmox, including server virtualization, backup,
  and email security, helps companies of any size, sector, or industry to
  simplify their IT infrastructures. The Proxmox solutions are based on the
  great Debian platform, and we are happy that we can give back to the
  community by sponsoring DebConf23.

- [**Siemens**](https://www.siemens.com/) is our third Platinum sponsor.
  Siemens is a technology company focused on industry, infrastructure and
  transport. From resource-efficient factories, resilient supply
  chains, smarter buildings and grids, to cleaner and more comfortable
  transportation, and advanced healthcare, the company creates
  technology with purpose adding real value for customers. By combining
  the real and the digital worlds, Siemens empowers its customers to
  transform their industries and markets, helping them to enhance the
  everyday of billions of people.

Our Gold sponsors are:

- [**Lenovo**](https://www.lenovo.com/),
  Lenovo is a global technology leader manufacturing a wide portfolio of
  connected products including smartphones, tablets, PCs and workstations
  as well as AR/VR devices, smart home/office and data center solutions.

- [**Freexian**](https://www.freexian.com/),
  Freexian is a services company specialized in Free Software and in
  particular Debian GNU/Linux, covering consulting, custom developments,
  support, training. Freexian has a recognized Debian expertise thanks to the
  participation of Debian developers.

- [**Google**](https://google.com/),
  Google is one of the largest technology companies in the world, providing a
  wide range of Internet-related services and products such as online
  advertising technologies, search, cloud computing, software, and hardware.

- [**Ubuntu**](https://www.canonical.com/),
  the Operating System delivered by Canonical.

Our Silver sponsors are:

- The [**Bern University of Applied Sciences**](https://www.bfh.ch/)
  with near [7,800](https://www.bfh.ch/en/bfh/facts_figures.html) students
  enrolled, located in the Swiss capital.
- [**Collabora**](https://www.collabora.com/), a global consultancy delivering
  Open Source software solutions to the commercial world.
- [**FOSS United**](https://fossunited.org/),
  a non-profit foundation that aims at promoting and strengthening the Free
  and Open Source Software (FOSS) ecosystem in India.
- The [**FSIJ**](https://www.fsij.org/), a non-profit organization
  dedicated to supporting Free Software growth and development.
- [**Civil Infrastructure Platform**](https://www.cip-project.org/),
  a collaborative project hosted by the Linux Foundation,
  establishing an open source “base layer” of industrial grade software.
- [**Roche**](https://www.roche.com/), a major international pharmaceutical
  provider and research company dedicated to personalized healthcare.
- [**Two Sigma**](https://www.twosigma.com/),
  rigorous inquiry, data analysis, and invention to help solve the toughest
  challenges across financial services.
- [**Matanel Foundation**](http://www.matanel.org/), operates in Israel,
  as its first concern is to preserve the cohesion of a society and a nation
  plagued by divisions.
- The [**FOSSEE**](https://fossee.in/) (Free/Libre and Open Source Software for
  Education) project promotes the use of FLOSS tools in academia and research.
  The project is part of the National Mission on Education through Information
  and Communication Technology (ICT), Ministry of Education (MoE), Government of
  India.
- [**Arm**](https://www.arm.com/): with the world’s Best SoC Design Portfolio,
  Arm powered solutions have been supporting innovation for more than 30 years
  and are deployed in over 225 billion chips to date.
- [**Kerala Vision Broadband**](https://www.keralavisionisp.com/) is a
  subsidiary of Kerala State IT Infrastructure Limited (KSITIL) which provides
  high-speed internet services in the state of Kerala, India.

Bronze sponsors:

- [**BigBinary**](https://www.bigbinary.com/),
- [**Zerodha**](https://zerodha.com/),
- [**passbolt**](https://www.passbolt.com/),
- [**Univention**](https://www.univention.com/),
- [**DeepRoot Linux**](https://deeproot.in/),
- [**Mostly Harmless**](https://mostlyharmless.io/debconf/),
- [**hopbox**](https://hopbox.net/).

And finally, our Supporter level sponsors:

- [**ETH Zurich**](https://ethz.ch/en.html),
- [**Gandi.net**](https://www.gandi.net/en-US),
- [**evolix**](https://evolix.com/),
- [**Hudson River Trading**](https://www.hudsonrivertrading.com/).

A special thanks to the [**Infoparks Kerala**](https://infopark.in/),
our Venue Partner!

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number
of Debian contributors from all over the globe to work together,
help and learn from each other in DebConf23.

