Title: Call for bids for DebConf24
Date: 2023-10-31 13:30
Tags: debconf, debconf24, debian
Slug: debconf24-call-for-bids
Author: Debian DebConf Committe
Status: published


Due to the current state of affairs in Israel, who were to host DebConf24,
the DebConf committee has decided to renew calls for bids to host DebConf24 
at another venue and location.

The DebConf committee would like to express our sincere appreciation for the
DebConf Israeli team, and the work they've done over several years. However,
given the uncertainty about the situation, we regret that it will most likely
not be possible to hold DebConf in Israel.

As we ask for submissions for new host locations we ask that you please review
and understand the details and requirements for a bid submission to host the
[Debian Developer Conference](https://www.debconf.org).

Please review the [template for a DebConf bid](https://wiki.debian.org/DebConf/General/Handbook/Bids/LocationCheckList)
for guidelines on how to sumbit a proper bid.

To submit a bid, please create the appropriate page(s) under
[DebConf Wiki Bids](https://wiki.debian.org/DebConf/24/Bids), and add it to the "Bids"
section in the main [DebConf 24](https://wiki.debian.org/DebConf/24) page. 


_There isn't very much time to make a decision. We need bids by the end of November
in order to make a decision by the end of the year._  

After your submission is completed please
send us a notification at [debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org) to let us know
that your bid submission is ready for review. 

We also suggest hanging out in our IRC chat room [#debconf-team](https://www.oftc.net). 

Given this short deadline, we understand that bids won't be as complete
as they would usually be. Do the best you can in the time available.

Bids will be evaluated according to [The Priority List](https://wiki.debian.org/DebConf/General/Handbook/Bids/PriorityList).

You can get in contact with the DebConf team by email to
[debconf-team@lists.debian.org](mailto:debconf-team@lists.debian.org), or via the #debconf-team IRC channel on
OFTC or via our [Matrix Channel](https://matrix.to/#/#debconf-team:matrix.debian.social).


Thank you, 

The Debian Debconf Committee

