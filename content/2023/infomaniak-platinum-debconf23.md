Title: Infomaniak First Platinum Sponsor of DebConf23
Slug: infomaniak-platinum-debconf23
Date: 2023-05-21 14:08
Author: Sahil Dhiman
Artist: Infomaniak
Tags: debconf23, debconf, sponsors, infomaniak
Image: /images/infomaniak.png
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com/)

We are pleased to announce that [**Infomaniak**](https://www.infomaniak.com/)
has committed to sponsor [DebConf23](https://debconf23.debconf.org) as a
**Platinum Sponsor**.

Infomaniak is a key player in the European Cloud and the leading developer of
Web technologies in Switzerland. It aims to be an independent European
alternative to the web giants and is committed to an ethical and sustainable
Web that respects privacy and creates local jobs. Infomaniak develops cloud
solutions (IaaS, PaaS, VPS), productivity tools for online collaboration and
video and radio streaming services.

The company uses only renewable electricity, offsets 200% of its CO2 emissions
and extends the life of its servers up to 15 years. The company cools its
infrastructure with filtered air, without air conditioning, and is building
a new data centre that will fully recycle the energy it consumes to heat up 
to 6,000 homes.

With this commitment as Platinum Sponsor, Infomaniak is contributing to make
possible our annual conference, and directly supporting the progress of
Debian and Free Software, helping to strengthen the community that continues
to collaborate on Debian projects throughout the rest of the year.

Thank you very much Infomaniak, for your support of DebConf23!

##Become a sponsor too!

[DebConf23](https://debconf23.debconf.org) will take place from September 10th
to 17th, 2022 in Kochi, India, and will be preceded by DebCamp, from
September 3rd to 9th.

And DebConf23 is accepting sponsors!
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf23 website at 
[https://debconf23.debconf.org/sponsors/become-a-sponsor/](https://debconf23.debconf.org/sponsors/become-a-sponsor/).
