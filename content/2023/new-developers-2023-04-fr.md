Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2023)
Slug: new-developers-2023-04
Date: 2023-05-25 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * James Lu (jlu)
  * Hugh McMaster (hmc)
  * Agathe Porte (gagath)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Soren Stoutner
  * Matthijs Kooijman
  * Vinay Keshava
  * Jarrah Gosbell
  * Carlos Henrique Lima Melara
  * Cordell Bloor

Félicitations !

