Title: Nowi Deweloperzy i Opiekunowie Debiana (lipiec i sierpień 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean‐Pierre Giraud
Tags: project
Lang: pl
Translator: Grzegorz Szymaszek
Status: published


Następujący współtwórcy otrzymali konta Deweloperów Debiana w&nbsp;ciągu
ostatnich dwóch miesięcy:

  * <span lang="no">Marius Gripsgard</span> (mariogrip)
  * <span lang="en">Mohammed Bilal</span> (rmb)
  * <span lang="de">Lukas Märdian</span> (slyon)
  * <span lang="sv">Robin Gustafsson</span> (rgson)
  * <span lang="pt">David da Silva Polverari</span> (polverari)
  * <span lang="es">Emmanuel Arias</span> (eamanu)

Następujący współtwórcy dołączyli do Opiekunów Debiana w&nbsp;ciągu ostatnich
dwóch miesięcy:

  * <span lang="fr">Aymeric Agon&shy;&#8209;Rambosson</span>
  * <span lang="en">Blair Noctis</span>
  * <span lang="en">Lena Voytek</span>
  * <span lang="fr">Philippe Coval</span>
  * <span lang="en">John Scott</span>

Gratulacje!
