Title: Nouveaux développeurs et mainteneurs de Debian (septembre et octobre 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator:
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * François Mazen (mzf)
  * Andrew Ruthven (puck)
  * Christopher Obbard (obbardc)
  * Salvo Tomaselli (ltworf)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Bo YU
  * Athos Coimbra Ribeiro
  * Marc Leeman
  * Filip Strömbäck

Félicitations !

