Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (novembro e dezembro de 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

O seguinte colaborador do projeto se tornara Mantenedor Debian nos últimos dois meses:

  * Safir Secerovic

Parabéns a todos!

