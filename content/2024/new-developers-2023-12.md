Title: New Debian Developers and Maintainers (November and December 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: en
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

The following contributor was added as Debian Maintainer in the last two months:

  * Safir Secerovic

Congratulations!

