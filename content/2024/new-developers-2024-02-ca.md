Title: Nous desenvolupadors i mantenidors de Debian (gener i febrer del 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Carles Pina i Estany (cpina)
  * Dave Hibberd (hibby)
  * Soren Stoutner (soren)
  * Daniel Gröber (dxld)
  * Jeremy Sowden (azazel)
  * Ricardo Ribalda Delgado (ribalda)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Joachim Bauch
  * Ananthu C V
  * Francesco Ballarin
  * Yogeswaran Umasankar
  * Kienan Stewart

Enhorabona a tots!

