Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Carles Pina i Estany (cpina)
  * Dave Hibberd (hibby)
  * Soren Stoutner (soren)
  * Daniel Gröber (dxld)
  * Jeremy Sowden (azazel)
  * Ricardo Ribalda Delgado (ribalda)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Joachim Bauch
  * Ananthu C V
  * Francesco Ballarin
  * Yogeswaran Umasankar
  * Kienan Stewart

Félicitations !

