Title: नए डेबियन डेवलपर्स और मेंटेनर्स (जनवरी और फ़रवरी 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published


पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

  * Carles Pina i Estany (cpina)
  * Dave Hibberd (hibby)
  * Soren Stoutner (soren)
  * Daniel Gröber (dxld)
  * Jeremy Sowden (azazel)
  * Ricardo Ribalda Delgado (ribalda)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

  * Joachim Bauch
  * Ananthu C V
  * Francesco Ballarin
  * Yogeswaran Umasankar
  * Kienan Stewart

इन्हें बधाईयाँ!

