Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (janeiro e fevereiro de 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * Carles Pina i Estany (cpina)
  * Dave Hibberd (hibby)
  * Soren Stoutner (soren)
  * Daniel Gröber (dxld)
  * Jeremy Sowden (azazel)
  * Ricardo Ribalda Delgado (ribalda)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Joachim Bauch
  * Ananthu C V
  * Francesco Ballarin
  * Yogeswaran Umasankar
  * Kienan Stewart

Parabéns a todos!

