Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng giêng và hai 2024)
Slug: new-developers-2024-02
Date: 2024-03-23 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Carles Pina i Estany (cpina)
  * Dave Hibberd (hibby)
  * Soren Stoutner (soren)
  * Daniel Gröber (dxld)
  * Jeremy Sowden (azazel)
  * Ricardo Ribalda Delgado (ribalda)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Joachim Bauch
  * Ananthu C V
  * Francesco Ballarin
  * Yogeswaran Umasankar
  * Kienan Stewart

Xin chúc mừng!

