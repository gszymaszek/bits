Title: Debian 25 anniversary
Date: 2018-08-16 17:50
Tags: debian, birthday
Slug: debian-25-anniversary
Author: draft

Today is Debian's 25th anniversary! 


<!-- we need a nice logo here ... -->

![Debian 25](|filename|/images/debian25.png)


The Debian Project was founded in 1993 by Ian Murdock to be a truly free
community project. Since then the project has grown to be one of the
largest and most influential open source projects. Thousands of
volunteers from all over the world work together to create and maintain
Debian software. Available in 70 languages, and supporting a large range
of computer types, Debian truly is  the "universal operating system".


<ul>
<li>Over 865,243,118 source lines of code between hamm released in 1998 and 
stretch released in 2017. Over 1,605,945,628 source lines of code between
hamm and sid.</li>

<li>There are over 300 derivatives based on Debian.</li>

</ul>


<!-- some facts about Debian -->


If you are close to any of the cities
[celebrating Debian Day 2018](https://wiki.debian.org/DebianDay/2018), you're
very welcome to join the party!

If not, there's still time for you to organize a little celebration or
contribution to Debian. 

<!-- paragraph about saying/sending thanks to Debian contributors, or
showing the logo in your webs etc to increase visibility of Debian -->

<!-- how to contribute or join -->


If you also like graphics design, or design in general, have a look at 
[https://wiki.debian.org/Design](https://wiki.debian.org/Design) and join
the team! Or you can visit the general list of [Debian Teams](https://wiki.debian.org/Teams)
for many other opportunities to participate in Debian development.

Thanks to everybody who has contributed to develop our beloved operating system
in these 25 years, and happy birthday Debian!
